#!/usr/bin/env python
import os, sys
import argparse
from loadData import *
import extract_lammps_data
import polymerNetwork
def is_valid(x):
    if not os.path.exists(x):
        print("The file %s does not exist!" % x)
        sys.exit("Error!!!")

def main():
        parser = argparse.ArgumentParser(description="doc",\
    		formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        parser.add_argument("-inputPath", "--inputPath",
                     help="path of the data file")
        parser.add_argument("-trajFile", "--trajectory",
                    default="pdump.lammpstrj",
                    help="trajectory file")
        parser.add_argument("-bondFile", "--bond",
                    default="pdump.bond",
                    help="bond file")
        args = parser.parse_args()
        pathToFile = args.inputPath
        is_valid(pathToFile)
        trajFile = args.trajectory 
        bondFile = args.bond
        #args = create_psf(args)
        print  (args.inputPath,trajFile,bondFile)
        owd = os.getcwd()
        os.chdir(pathToFile)
        #outFile = 'RAFT75_5050000.data' 
        outFile = dumptodatafile(owd,pathToFile,trajFile,bondFile)
        #psfOutput = create_psf(outFile)
        #cmd2 ="python " + owd + "/PostProcess/extract_lammps_data.py \"Bonds\"  <%s> bonds.dat" %outFile
        #print cmd2
        #os.system(cmd2)
        #cmd2 ="python " + owd + "/PostProcess/extract_lammps_data.py \"Atoms\"  <%s> atoms.dat" %outFile
        #print cmd2
        #os.system(cmd2)
        #G = generatePolymerNetworkGraph()
        #print "generated Graph"
        #extract_lammps_data.main("Bonds",outFile,'bonds.dat')
        os.chdir(owd)
        #return (pathToFile,trajFile,bondFile)
        return None
if __name__ == '__main__':
   main()
