import networkx as nx
import random
#nx.__version__

inimerTypes = [1]
crosslinkerTypes = [18,19,21]
chainEndTypes = [15]

def generatePolymerNetworkGraph(atomID,atomType,x,y,z):
#def generatePolymerNetworkGraph():
    fbonds = open('bonds.dat', 'r')  #<--contains only the Bonds section
    G=nx.Graph()
    for line in fbonds:
        tokens=line.strip().split()
        if len(tokens) == 0:
            continue
        # The bond-type is in column 2.
        bondtype = int(tokens[1])
        # The atom-IDs are in columns 3 and 4.
        atomid1 = int(tokens[2])
        atomid2 = int(tokens[3])
        G.add_edge(atomid1, atomid2, type=bondtype)
    #readAtoms()
    #atomID,atomType,x,y,z = readAtoms()
    #print atomID,atomType,x,y,z
    for n in G.nodes_iter():
       for i in range(len(atomID)):
          if atomID[i] == n:
            G.node[n]['type'] =  atomType[i]
            G.node[n]['x'] =  x[i]
            G.node[n]['y'] =  y[i]
            G.node[n]['z'] =  z[i]
            break
    #for n in G.nodes_iter(data=True):
    #   print n
    inimerIndexes = chainLengthBetweenCrosslinks(G)          
    return inimerIndexes


def readAtoms():
    fatoms = open('atoms.dat', 'r')  #<--contains only the Atoms section
    atomID = []
    atomType = []
    x,y,z = [], [], []
    for line in fatoms:
        tokens=line.strip().split()
        if len(tokens) == 0:
            continue
        # The atom-ID is in column 1.
        atomID.append(int(tokens[0]))
        # The atom-type,coods are in columns 2,4,5 & 6.
        atomType.append(int(tokens[1]))
        x.append(float(tokens[3]))
        y.append(float(tokens[4]))
        z.append(float(tokens[5]))
    #print atomID,atomType,x,y,z
    return atomID,atomType,x,y,z


def chainLengthBetweenCrosslinks(H):
    G = H.copy()
    crosslinkers = [n for n, d in G.nodes(data=True) 
			if d['type'] in [18,19,21]]
    #for i in crosslinkers:
    #   print i, G.node[i]['type']
    #print len(crosslinkers)
    remainingCrosslinks = list(crosslinkers)
    #remainingCrosslinks.pop(0)
    chains = []
    for cl in crosslinkers:
        #print "neighbors: ", G.neighbors(cl)
        cnt = 0
        while cnt <= len(G.neighbors(cl)):
            tmp = [cl]
            edgesToRemove = []
            for i in nx.traversal.edge_dfs(G,cl):
               edgesToRemove.append(i)
               #print i, G.node[i[0]]['type'],G.node[i[1]]['type']
               tmp.append(i[1])
               #if any(c in remainingCrosslinks for c in i):
               if i[1] in remainingCrosslinks:
                  break
            G.remove_edges_from(edgesToRemove)
            cnt += 1
            if (len(tmp)> 1):
		 chains.append(tmp)       
            #print tmp
    print len(chains)
    listToRemove = []
    for i in range(len(chains)):
       if (G.node[chains[i][-1]]['type'] not in [18,19,21]) or (chains[i][0]== chains[i][-1]) or (len(chains[i])< 8):
         print chains[i]
         listToRemove.append(i)
    #for i in listToRemove:
       #chains.pop(i)
    newlist = [v for i, v in enumerate(chains) if i not in listToRemove]
    print len(newlist)
    totalInimers = 120
    randomInimerPlacement = random.sample(range(len(newlist)), totalInimers)
    inimerID = []
    for i in randomInimerPlacement:   
       index = random.randint(0,len(newlist[i])) - 1
       inimerID.append(newlist[i][index])
    print "InimerIndex: ", inimerID
    print "# inimers: ", len(inimerID)
    return inimerID
         
