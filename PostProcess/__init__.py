"""
DPDAnalysis
===========

     DPDAnalysis (DPDA) is a Python package to postprocess or analyse the DPD simulation results of polymer gels.
Using
-----

    Just write in Python

    >>> import DPDAnalysis as dpda
    >>> 

"""
#    Copyright (C) 2017 by
#    Santidan Biswas <santidanbiswas@gmail.com>
#    All rights reserved.
#    --- license.
#
# Add platform dependent shared library path to sys.path
#

from __future__ import absolute_import

import sys
if sys.version_info[:2] < (2, 7):
    m = "Python 2.7 or later is required for NetworkX (%d.%d detected)."
    raise ImportError(m % sys.version_info[:2])
del sys

# Release data
#from DPDAnalysis import release

#__author__ = '%s <%s>\n%s <%s>\n%s <%s>' % \
#    (release.authors['Biswas'] + release.authors[''] +
#        release.authors[''])
#__license__ = release.license
#
#__date__ = release.date
#__version__ = release.version
#
#__bibtex__ = """@inproceedings{,
#author = {},
#title = {},
#year = {},
#month = ,
#urlpdf = {},
#booktitle = {},
#editors = {},
#address = {},
#pages = {}
#}


#check python version
#import platform
#import os, matplotlib and numpy
#import os
#%matplotlib inline
#import matplotlib.pyplot as plt
#import numpy
#check MDAnalysis version
#import MDAnalysis
#print "Python Version: ", platform.python_version()
#print "MDAnalysis Version:  ", MDAnalysis.__version__


