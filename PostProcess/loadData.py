import os
import sys
import extract_lammps_data
sys.path.insert(0, "/home/santidan/opt/pizza-9Oct15/src/")
from dump import dump
from data import data
from xyz import xyz
from printFormat import *
from polymerNetwork import *

def dumptodatafile(owd,pathToData,trajFile,bondFile):
    print(trajFile,bondFile)
    #load the lammps trajectory file in pizza dump
    d = dump(trajFile)
    d.map(1,"id",2,"type",3,"mol", 4,"x",5,"y",6,"z")
    #load the bond datafile in the pizza dump
    b = dump(bondFile)
    b.map(1,"id",2,"type",3,"atom1", 4,"atom2") 
    # box dimensions required for writing data file
    time,box,atoms,bonds,tris,lines = d.viz(1)
    d.sort()
    #final time snapshot value
    timeStep=d.time()[-1]
    # creating a list to lammps xyz
    id, type, mol,x,y,z = d.vecs(timeStep,"id", "type","mol" ,"x","y","z")
    id = map(int,id)
    type = map(int,type)
    mol = map(int,mol)
    x = map(prettyfloat, x)
    y = map(prettyfloat, y)
    z = map(prettyfloat, z)
    # creating a list of bonds
    bid, btype, batom1,batom2 = b.vecs(timeStep,"id","type","atom1","atom2")
    bid = map(int,bid)
    btype = map(int,btype)
    batom1 = map(int,batom1)
    batom2 = map(int,batom2)
    # Merging lammpstrj and bond information of a given snapshot to create a data file
    dd = data()
    dd.title = "My LAMMPS data file" #set title of the data file
    nAtoms = len(id)
    nBonds = len(bid)
    atomTypes=len(set(type))
    bondTypes=len(set(btype))
    dd.headers["atoms"] =  nAtoms
    dd.headers["bonds"] =  nBonds
    dd.headers["atom types"] =  atomTypes
    dd.headers["bond types"] =  bondTypes
    dd.headers["xlo xhi"] = (box[0], box[3]) 
    dd.headers["ylo yhi"] = (box[1], box[4]) 
    dd.headers["zlo zhi"] = (box[2], box[5]) 
    l = [id, type, mol,x,y,z]
    op = map(list, zip(*l))
    xd = printLammpsSection(op)
    b = [bid, btype, batom1,batom2]
    op1 = map(list, zip(*b))
    binfo = printLammpsSection(op1)
    dd.sections["Atoms"] =  xd #numpy.asarray(l).T.tolist()
    l2=[bid, btype, batom1,batom2]
    dd.sections["Bonds"] =  binfo #map(list, zip(*l2)) #numpy.asarray(l2).T.tolist()
    prefixName = "RAFT75_" + str(timeStep) 
    outFile = prefixName + ".data"
    dd.write(outFile)
    #creating the xyz file
    xyz1=xyz(d)
    xyz1.single(timeStep,prefixName)
    psfOutput = create_psf(outFile)
    cmd2 ="python " + owd + "/PostProcess/extract_lammps_data.py \"Bonds\"  <%s> bonds.dat" %outFile
    print cmd2
    os.system(cmd2)
    cmd2 ="python " + owd + "/PostProcess/extract_lammps_data.py \"Atoms\"  <%s> atoms.dat" %outFile
    print cmd2
    os.system(cmd2)
    inimerIndex = generatePolymerNetworkGraph(id,type,x,y,z)
    for i in inimerIndex:
	if i in id:
          type[id.index(i)] = 31
          #print "Matched!", i, id.index(i), type[id.index(i)]
    l = [id, type, mol,x,y,z]
    op = map(list, zip(*l))
    xd = printLammpsSection(op)
    prefixName = "RAFT75_Inimer_" + str(timeStep) 
    outFile = prefixName + ".data"
    dd.sections["Atoms"] =  xd #numpy.asarray(l).T.tolist()
    dd.write(outFile)
    #matching = [i for i, x in enumerate(id) if any(thing in x for thing in inimerIndex)] 
    #print len(matching)
    return outFile


def create_psf(args):
    """
    given data file produce psf file if it doesn't exist yet
    if it does then use it
    """

    if  os.path.exists(os.path.abspath(args)):
        psffile = os.path.splitext(args)[0]
        PSF = psffile+'.psf'
        DCD = os.path.splitext(args)[0] + '.dcd'
        DATA = os.path.splitext(args)[0] + '.data'

        vmdscript = "create_psf.vmd"

        # write VMD loader script
        parameters = {'vmdfile': vmdscript,
                      'topology': PSF,
                      'datafile': DATA,
                      'trajectory': DCD
                     }

        script = """\
            package require topotools
            topo readlammpsdata "{0[datafile]}" angle
            animate write psf "{0[topology]}"
        exit
        """.format(parameters)


        with open(vmdscript, 'w') as tcl:
            tcl.write(script+'\n')

        os.system("vmd -dispdev text -e {0[vmdfile]}".format(parameters))

        print "Wrote VMD script {0}  ".format(vmdscript)
        print "If there is an error with {0}: 'source {0}' to load everything manually, then repeat ".format(vmdscript)
        print "running the python script with explicict parameters that were generated".format(vmdscript)
    else:
        print "the file %s does not exist" % args


    return  (os.path.splitext(args)[0] + '.psf')


