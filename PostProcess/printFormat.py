def printLammpsSection(Matrix):
    """
       formatting lists to section for use in pizza.py data output
    """
    string1 = ("\n".join(( str(row)[1:-1].replace(",", " ")) for row in Matrix))
    return [string1]

class prettyfloat(float):
    """
       formatting floating point numbers to 5 decimal places 
    """
    def __repr__(self):
        return "%0.5f" % self
