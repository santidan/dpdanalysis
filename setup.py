#! /usr/local/bin/python
try:
   from setuptools import setup
except ImportError:
   from distutils.core import setup


config = {
   'description': 'My Project',
   'author': 'Santidan Biswas',
   'url':'',
   'author_email': 'santidanbiswas+dpdaATgmail.com',
   'version': '0.1',
   'install_requires': ['nose'],
   'packages': ['NAME'],
   'scripts': [],
   'name': 'PostProcess'
   'entry_points':{
          'console_scripts': [
              'PostProcess = PostProcess.__main__:main'
          ]},
}

setup(**config)
